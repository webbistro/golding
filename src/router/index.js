import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/finance',
    name: 'finance',
    meta: {layout: 'main'},
    component: () => import('../views/Finance.vue')
  },
  {
    path: '/friend',
    name: 'friend',
    meta: {layout: 'main'},
    component: () => import('../views/Friend.vue')
  },
  {
    path: '/inx',
    name: 'inx',
    meta: {layout: 'main'},
    component: () => import('../views/Inx.vue')
  },
  {
    path: '/news',
    name: 'news',
    meta: {layout: 'main'},
    component: () => import('../views/News.vue')
  },
  {
    path: '/questions',
    name: 'questions',
    meta: {layout: 'main'},
    component: () => import('../views/Questions.vue')
  },
  {
    path: '/resources',
    name: 'resources',
    meta: {layout: 'main'},
    component: () => import('../views/Resources.vue')
  },
  {
    path: '/services',
    name: 'services',
    meta: {layout: 'main'},
    component: () => import('../views/Services.vue')
  },
  {
    path: '/shop',
    name: 'shop',
    meta: {layout: 'main'},
    component: () => import('../views/Shop.vue')
  },
  {
    path: '/academia',
    name: 'academia',
    meta: {layout: 'main'},
    component: () => import('../views/Academia.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
